import React, { Component } from 'react';
import autoBind from 'react-autobind';
import strings from '../services/strings';
import { Link } from 'react-router';

import './Sidebar.scss';

class Sidebar extends Component {

    constructor(props) {
        super(props);
        autoBind(this);
    }

    getLinkClassName(linkName) {
        let path = location.pathname.split('/');
        return (path[1] == linkName) ? 'active' : '';
    }

    handleItemClick() {
        this.props.toggleSidebar();
    }

    handleLogoutClick() {
        this.props.logout();
    }

    render() {
        return (
            <span>
                <div className="title">
                    <h1>Juridocs</h1>
                </div>
                <ul className="nav nav-sidebar">
                    <li className={ this.getLinkClassName('documents') } onClick={ this.handleItemClick }>
                        <Link to="/documents" href="#">Documents</Link>
                    </li>
                    <li className={ this.getLinkClassName('glossaries') } onClick={ this.handleItemClick }>
                        <Link to="/glossaries" href="#">Glossaries</Link>
                    </li>
                    <li className={ this.getLinkClassName('categories') } onClick={ this.handleItemClick }>
                        <Link to="/categories" href="#">Categories</Link>
                    </li>
                    <li className={ this.getLinkClassName('companies') } onClick={ this.handleItemClick }>
                        <Link to="/companies" href="#">Companies</Link>
                    </li>
                    <li className={ this.getLinkClassName('grammar') } onClick={ this.handleItemClick }>
                        <Link to="/grammar/gender-strings" href="#">Grammar</Link>
                    </li>
                    <li className={ this.getLinkClassName('users') } onClick={ this.handleItemClick }>
                        <Link to="/users" href="#">Users</Link>
                    </li>
                    <li className={ this.getLinkClassName('settings') } onClick={ this.handleItemClick }>
                        <Link to="/settings/languages" href="#">Settings</Link>
                    </li>
                    <li className="logout" onClick={ this.handleItemClick }>
                        <a href="#" onClick={ this.handleLogoutClick }>Logout</a>
                    </li>
                </ul>
            </span>
        );
    }

}

Sidebar.propTypes = {
    toggleSidebar: React.PropTypes.func.isRequired,
}

export default Sidebar;