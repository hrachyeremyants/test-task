import React, { Component } from 'react';
import autoBind from 'react-autobind';
import strings from '../../services/strings';
import language from '../../services/language';
import _ from 'lodash';

import './DocumentCategoryFilter.scss';

class DocumentCategoryFilter extends Component {

    constructor(props) {
        super(props);
        autoBind(this);
    }
    getCategories() {
        if (this.props.categories) {
           return  _.map(this.props.categories, (item)=>{
                return (
                    <option value={ item.name } key={ item.id }>
                        {`${item.name}`}
                    </option>
                )
            });
        }
    }

    getSelectedCategory() {
        if (this.props.filters) {
            return this.props.filters.categoryId;
        }

        return '';
    }

    handleChange(e) {
        this.props.setCategoryId(e.target.value);
        this.props.fetchItems(true);
    }

    render() {
        console.log(this.getCategories());

        return (
            <div className="DocumentCategoryFilter">
                <label>From</label>
                <select className="form-control" name="groups" value={ this.getSelectedCategory() } onChange={ this.handleChange }>
                    { this.getCategories() }
                </select>
            </div>
        );
    }

}

DocumentCategoryFilter.propTypes = {
    // filters: React.PropTypes.object,
    // categories: React.PropTypes.array,
    // fetchItems: React.PropTypes.func.isRequired,
}

export default DocumentCategoryFilter;