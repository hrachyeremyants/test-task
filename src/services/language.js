import api from './api';

class LanguageService {

	defaultLanguage = '2';

	// constructor() {
	// 	if (!this.languages) {
	// 		this.getLanguages();
	// 	}
	// }

	get() {
		let language = localStorage.getItem('language');

		if (!language) {
			language = this.defaultLanguage;
			this.set(language);
		}

		return language;
	}

	set(language) {
		localStorage.setItem('language', language);
	}
}

export default new LanguageService();