import auth from './auth';
import exception from './exception';
import config from '../config'

const API_ENDPOINT = config.API_ENDPOINT;

class ApiService {

	async get(resource, params = new Map(), withResponse = false,category = false) {
		let options = {
			method: 'GET',
	      	headers: {
	        	'Accept': 'application/json',
	        	'Content-Type': 'application/x-www-form-urlencoded'
	      	}
		};

		let url = API_ENDPOINT + resource + '?' + this.serializeParams(params);

		let [response, data] = await this.request(url, options);
		if (!response.ok) {
			return exception.throwFromResponse(data);
		}
		if (category){
            return withResponse ? [response, data] : data;
		}
		return withResponse ? [response, data.results] : data.results;
	}

	async request(url, options, accessToken) {
		if (accessToken) {
			options.headers['Authorization'] = `Bearer ${accessToken}`;
		}
		else if (auth.isAuthenticated()) {
			accessToken = await auth.getAccessToken();
			options.headers['Authorization'] = `Bearer ${accessToken.value}`;
		}

		let response = await fetch(url, options);
		let data = {};

		try {
			data = await response.json();
		} catch(e) {
			// nothing to do here
		}
		return [response, data];
	}

	serializeParams(params) {
		let serialized = '';
		let array = [];

		params.forEach((value, key) => {
			array.push(key + '=' + encodeURIComponent(value));
		});

		return array.join('&');
	}

}

export default new ApiService();