import documents from './documents/reducer';

import categories from './categories/reducer';

export {
    documents,
    categories
};
