import { browserHistory } from 'react-router';
import api from '../../services/api';
import _ from 'lodash';

import * as categoriesSelectors from './selectors';

export const types = {
	FETCH_ALL_ITEMS_DONE: 'categories.FETCH_ALL_ITEMS_DONE',

	FETCH_ITEMS_DONE: 'categories.FETCH_ITEMS_DONE',
	FETCH_ITEM_DONE: 'categories.FETCH_ITEM_DONE',
	SET_SEARCH_TERM: 'categories.SET_SEARCH_TERM',
	SET_CURRENT_PAGE: 'categories.SET_CURRENT_PAGE',
	SET_CURRENT_ITEM_ID: 'categories.SET_CURRENT_ITEM_ID',
	TOGGLE_SORTER: 'categories.TOGGLE_SORTER',
	CLEAR_CACHE: 'categories.CLEAR_CACHE',
};

export function setCurrentPage(page) {
	return {
		type: types.SET_CURRENT_PAGE,
		payload: {
			page
		}
	}
}

export function setCurrentItemId(id) {
	return {
		type: types.SET_CURRENT_ITEM_ID,
		payload: {
			id
		}
	}
}

export function unsetCurrentItemId() {
	return {
		type: types.SET_CURRENT_ITEM_ID,
		payload: {
			id: null,
		}
	}
}

export function toggleSorter(column) {
	return {
		type: types.TOGGLE_SORTER,
		payload: {
			column
		}
	}
}

export function setSearchTerm(searchTerm) {
	return {
		type: types.SET_SEARCH_TERM,
		payload: {
			searchTerm
		}
	}
}

export function clearCache() {
	return {
		type: types.CLEAR_CACHE
	}
}

export function fetchAllItems() {
	return async (dispatch) => {
		try {
			let params = new Map();
			// params.set('language_id', language.get());

			let items = await api.get('/', params,false,true);
			console.log(items,'items');

			dispatch(clearCache());
			dispatch({
				type: types.FETCH_ALL_ITEMS_DONE,
				payload: {
					items
				}
			});
		} catch (e) {
			dispatch(exceptionsActions.process(e));
		}
	}
}

export function fetchItems(deleteCache = false) {
	return async (dispatch, getState) => {
		let state = getState();
		try {
			// Set additional params
			let params = new Map();
			let filters = categoriesSelectors.getFilters(state);
			let sorter = categoriesSelectors.getSorter(state);
			let pagination = categoriesSelectors.getPagination(state);
			// params.set('language_id', language.get());
			// params.set('expand', 'users,groups');
			// params.set('name~', filters.searchTerm);
			// params.set('page_size', pagination.pageSize);
			// params.set('page_number', deleteCache ? 1 : pagination.currentPage);
			// params.set('sort_by', sorter.column);
			// params.set('sort_desc', sorter.descending);


			// GET request from API
			let [response, items] = await api.get('/', params,false,true);

            // Clear cache if deleteCache is enabled
			if (deleteCache) {
				dispatch(clearCache());
			}

			dispatch({
				type: types.FETCH_ITEMS_DONE,
				payload: {
					totalPages: parseInt(response.headers.get('X-Total-Pages')),
					items
				}
			});
		} catch (e) {
			dispatch(exceptionsActions.process(e));
		}
	}
}

export function fetchItem(id) {
	return async (dispatch) => {
		try {
			let params = new Map();
			params.set('expand', 'users,groups');
			// GET request from API
			let item = await api.get(`/${id}`, params);
			dispatch({
				type: types.FETCH_ITEM_DONE,
				payload: {
					item
				}
			})
		} catch (e) {
			dispatch(exceptionsActions.process(e));
		}
	}
}