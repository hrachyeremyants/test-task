import classNames from 'classnames';
import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import strings from './services/strings';
import auth from './services/auth';
import './App.scss';
import './AppTabletLandscape.scss';
import './AppTabletPortret.scss';
import './AppMobile.scss';

class App extends Component {

    constructor(props) {
        super(props);
        autoBind(this);
    }

    componentWillMount() {
        console.log(123);
    }

    getClassName() {
        if (this.props.sidebarOpened) {
            return 'App sidebar-opened';
        }

        return 'App';
    }

    handleSidebarIconClick() {
        this.props.toggleSidebar();
    }

    render() {
        let children = this.props.children;

        return (
            <div className={ this.getClassName() }>
                <div className="container transation-2s-in-out">
                    { children }
                </div>
            </div>
        );
    }
}

export default App;