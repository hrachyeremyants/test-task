import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import strings from '../services/strings';
import language from '../services/language';
import { Link } from 'react-router';
import './LanguageSelector.scss';

class LanguageSelector extends Component {

    state = {
        language: language.get()
    }

    constructor(props) {
        super(props);
        autoBind(this);
    }

    componentWillMount() {
        this.props.fetchAllLanguages();
    }

    handleChange(e) {
        language.set(e.target.value);

        this.props.fetchCategories(true);
        this.props.fetchDocuments(true);
        this.props.fetchGlossaries(true);
        this.props.fetchGenderStrings(true);
        this.props.fetchSteps(true);

        this.setState({
            language: language.get()
        });
    }

    render() {
        return (
            <span className="LanguageSelector">
                <div className="form-group">
                    <select className="form-control" value={ this.state.language } onChange={ this.handleChange }>
                        <option value="2">Dutch</option>
                        <option value="1">English</option>
                    </select>
                </div>
            </span>
        );
    }

}

function mapStateToProps(state) {
    return {
        // languages: languagesSelectors.getItems(state),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchAllLanguages: () => {
            // dispatch(languagesActions.fetchAllItems())
        },
        fetchDocuments: (deleteCache) => {

            // dispatch(documentsActions.fetchItems(deleteCache))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSelector);