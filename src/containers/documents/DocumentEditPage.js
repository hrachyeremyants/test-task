import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import strings from '../../services/strings';
import { Link } from 'react-router';
import '../Page.scss';

import * as documentsActions from '../../store/documents/actions';
import * as documentsSelectors from '../../store/documents/selectors';

import Topbar from '../../components/Topbar';
import SubTopbar from '../../components/SubTopbar';
import DocumentForm from '../../components/document/DocumentForm';
import { getModal, getTemplateData } from '../../components/document/DocumentForm';
import _ from 'lodash';

class DocumentEditPage extends Component {

    state = {
        fieldsLoaded : false,
    };

    constructor(props) {
        super(props);
        autoBind(this);
    }

    componentWillMount() {
        this.props.fetchDocument(this.props.params.id);
        this.props.setCurrentDocumentId(this.props.params.id);
    }

    componentWillUnmount() {
        this.props.unsetCurrentDocumentId();
        this.props.clearExceptions();
    }

    saveDocument(data) {
        this.props.updateDocument(this.props.params.id, data.form);
        if (data.file) {
            this.props.uploadDocumentLogo(this.props.params.id, data.file);
        }
    }

    updateItemOrder(data){
        this.props.updateDocumentOrder(this.props.params.id, data);
    }

    validateDocument(data){
        this.props.validateDoc(this.props.params.id, data);
    }

    showOrderFieldsModal(){
        let modal = getModal();
        if(!(this.props.exceptions.template && this.props.exceptions.template.notDefined))
        modal.show();
    }

    handleOrderFieldsClick(){
        this.setState({fieldsLoaded: false});
        let templateData = getTemplateData();
        this.validateDocument(templateData);
        this.setState({fieldsLoaded: true});
        _.delay(()=>{this.showOrderFieldsModal()},250);
    }

    render() {
        return (
            <div className="DocumentEditPage">
                <Topbar>
                    <div className="title">
                        <Link to="/documents">Documents</Link>
                        <span className="hidden-xs">

                            <span className="divider">/</span>
                            <Link to={`/documents/${this.props.params.id}`}>Edit</Link>
                        </span>
                    </div>
                    <div className="main-btns">
                        <button className="btn btn-default" onClick={ this.handleOrderFieldsClick }>Order Fields</button>
                    </div>
                </Topbar>

                <div className="content">
                    <DocumentForm
                        exceptions={ this.props.exceptions }
                        categories={ this.props.categories }
                        glossaries={ this.props.glossaries }
                        currentItem={ this.props.currentDocument }
                        saveItem={ this.saveDocument }
                        fields={ this.props.fields }
                        fieldsOrder={ this.props.fieldsOrder }
                        selectors={ this.props.selectors }
                        clauses={ this.props.clauses }
                        steps={ this.props.steps }
                        stepsFromValidation={ this.props.stepsFromValidation }
                        updateItemOrder={ this.updateItemOrder }
                        updateGlossary={ this.props.updateGlossary }
                        createGlossary={ this.props.createGlossary }
                    />
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        currentDocument: documentsSelectors.getCurrentItem(state),
        fields: documentsSelectors.getFields(state),
        fieldsOrder: documentsSelectors.getFieldsOrder(state),
        selectors: documentsSelectors.getSelectors(state),
        clauses: documentsSelectors.getClauses(state),
        stepsFromValidation: documentsSelectors.getSteps(state),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchDocument: (id) => {
            dispatch(documentsActions.fetchItem(id))
        },
        setCurrentDocumentId: (id) => {
            dispatch(documentsActions.setCurrentItemId(id))
        },
        unsetCurrentDocumentId: () => {
            dispatch(documentsActions.unsetCurrentItemId())
        },
        updateDocument: (id, data) => {
            dispatch(documentsActions.updateItem(id, data))
        },
        uploadDocumentLogo: (id, file) => {
            dispatch(documentsActions.uploadItemLogo(id, file))
        },
        createDocument: (data) => {
            dispatch(documentsActions.createItem(data))
        },
        validateDoc: (id,data) => {
            dispatch(documentsActions.validateItem(id, data))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DocumentEditPage);