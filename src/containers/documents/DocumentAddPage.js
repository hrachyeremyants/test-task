import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import strings from '../../services/strings';
import { Link } from 'react-router';
import '../Page.scss';

import * as documentsActions from '../../store/documents/actions';
import * as documentsSelectors from '../../store/documents/selectors';

import Topbar from '../../components/Topbar';
import SubTopbar from '../../components/SubTopbar';
import DocumentForm from '../../components/document/DocumentForm';

class DocumentAddPage extends Component {

    constructor(props) {
        super(props);
        autoBind(this);
    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    saveDocument(data) {
        if (data.file) {
            this.props.createDocumentWithLogo(data.form, data.file);
        } else {
            this.props.createDocument(data.form);
        }
    }

    render() {
        return (
            <div className="DocumentAddPage">
                <Topbar>
                    <div className="title">
                        <Link to="/documents">Documents</Link>
                        <span className="hidden-xs">
                            <span className="divider">/</span>
                            <Link to="/documents/add">Add</Link>
                        </span>
                    </div>
                </Topbar>

                <div className="content">
                    <DocumentForm
                        exceptions={ this.props.exceptions }
                        categories={ this.props.categories }
                        saveItem={ this.saveDocument }
                    />
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        //categories: categoriesSelectors.getItems(state),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createDocument: (data) => {
            dispatch(documentsActions.createItem(data))
        },
        createDocumentWithLogo: (data, file) => {
            dispatch(documentsActions.createItemWithLogo(data, file))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DocumentAddPage);